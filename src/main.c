#include "game.h"
#include "computer/computer.h"
#include "ui/ui.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

/*****************************************************
 * Function: kill_self
 * Purpose: Clean close on SIGINT
 * Parameters: None
 * Returns: None
 ****************************************************/
void kill_self() {
	clear_computer();
	ui_quit();
	exit(0);
}

int main(int argc, char **argv) {
	struct sigaction redirect;
	memset(&redirect, 0, sizeof(struct sigaction));
	redirect.sa_handler = &kill_self;
	sigaction(SIGINT, &redirect, get_old_sigaction());
    
	
	ui_init();

    int menu_choice;

    while ((menu_choice = ui_main_menu()) != MENU_QUIT) {
        switch (menu_choice) {
            case MENU_PLAY:
                play_game();
                break;
            case MENU_SETTINGS:
                ui_settings();
                break;
            default:
                break;
        }
    }

    ui_quit();

    return 0;
}