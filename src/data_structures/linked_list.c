#include "linked_list.h"


/*****************************************************
 * Function: new_c_list
 * Purpose: Creates a new chained list
 * Parameters: None
 * Returns: The new chained list
 *****************************************************/
linked_list * new_c_list() { return calloc(1, sizeof(linked_list)); }


/*****************************************************
 * Function: new_c_list_node
 * Purpose: Creates a new chained list node
 * Parameters: None
 * Returns: The new chained list node
 *****************************************************/
linked_list_node * new_c_list_node() { return calloc(1, sizeof(linked_list_node)); }


/*****************************************************
 * Function: add_list
 * Purpose: Adds a node to the beginning of the list
 * Parameters: linked_list *list - the list to add to
 *             pid_t elt - the element to add
 * Returns: None
 *****************************************************/
void add_list(linked_list* list, pid_t elt) {
	linked_list_node *new = new_c_list_node();
	new->next = list->first;
	new->pid = elt;
	list->first = new;
}


/*****************************************************
 * Function: clear_list
 * Purpose: Clears the list
 * Parameters: linked_list *list - the list to clear
 * Returns: None
 *****************************************************/
void clear_list(linked_list* list) {
	linked_list_node *curr = list->first;

	while (curr) {
		linked_list_node *next = curr->next;
		free(curr);
		curr = next;
	}

    free(list);
}
