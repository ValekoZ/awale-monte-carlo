#ifndef HASH_MAP_H
#define HASH_MAP_H


#include "../game.h"

#include <stdlib.h>
#include <unistd.h>

#define HASH_MAP_SIZE 1 << 8


typedef struct hash_map_entry {
    game *key;                                  // We use a game position as the key
    struct hash_map_entry *next_positions[BOARD_SIZE / 2];      // We need to store all the next possible positions, after one possible move (max BOARD_SIZE / 2)
    int n_next_positions;       // How many next positions we actually have
    int n_wins;                 // How many times this position has been won
    int n_loses;                // How many times this position has been lost
    int n_games;                // How many times this position has been played
    bool is_complete;           // true if we played every game from this position
    struct hash_map_entry *next;    // Next entry in the hash map
} hash_map_entry;


typedef hash_map_entry *hash_map[HASH_MAP_SIZE];


hash_map *hash_map_new();


void hash_map_free(hash_map *h);


hash_map_entry *hash_map_get(hash_map *h, game *g);


hash_map_entry *hash_map_put(hash_map *h, game *g);


void hash_map_remove(hash_map *h, game *g);


uint8_t hash_map_hash(game *g);

#endif