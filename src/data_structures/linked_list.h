#ifndef CHAINED_LIST_H
#define CHAINED_LIST_H

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

typedef struct _linked_list_node {
	pid_t pid;
	struct _linked_list_node *next;
} linked_list_node;

typedef struct
{
	linked_list_node *first;
} linked_list;

linked_list *new_c_list();
linked_list_node *new_c_list_node();

void add_list(linked_list* list, pid_t elt);

void clear_list(linked_list* list);

#endif