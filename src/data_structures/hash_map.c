#include "hash_map.h"
#include <string.h>


/*****************************************************
 * Function: hash_map_new
 * Purpose: Creates a new hash map
 * Parameters: None
 * Returns: A pointer to the new hash map
 ****************************************************/
hash_map *hash_map_new() {
    return calloc(1, sizeof(hash_map));
}


/*****************************************************
 * Function: hash_map_free
 * Purpose: Frees the memory allocated to a hash map
 * Parameters: A pointer to the hash map to free
 * Returns: None
 ****************************************************/
void hash_map_free(hash_map *hm) {
    if (hm == NULL)
        return;

    // For each entry in the hash map, free the memory
    for (int i = 0; i < HASH_MAP_SIZE; i++) {
        hash_map_entry *entry = (*hm)[i];
        while (entry != NULL) {
            hash_map_entry *next = entry->next;
            game_free(entry->key);
            free(entry);
            entry = next;
        }
    }

    free(hm);
}


/*****************************************************
 * Function: hash_map_get
 * Purpose: Gets the value of a key in the hash map
 * Parameters: A pointer to the hash map, the key
 * Returns: The value of the key
 * Notes: If the key is not found, returns NULL
 ****************************************************/
hash_map_entry *hash_map_get(hash_map *hm, game *g) {
    uint8_t hash = hash_map_hash(g);
    hash_map_entry *entry = *hm[hash];
    
    // Iterate through the linked list until we find the key
    while (entry != NULL) {
        if (memcmp(entry->key, g, sizeof(game)) == 0) {
            return entry;
        }
        entry = entry->next;
    }
    
    return NULL;
}


/*****************************************************
 * Function: hash_map_put
 * Purpose: Puts a key in the hash map
 * Parameters: A pointer to the hash map, the key
 * Returns: None
 ****************************************************/
hash_map_entry *hash_map_put(hash_map *hm, game *g) {
    uint8_t hash = hash_map_hash(g);
    hash_map_entry *entry = *hm[hash];

    hash_map_entry *new_entry;

    // If the key is already in the hash map, we don't need to do anything
    if ((new_entry = hash_map_get(hm, g)) != NULL) {
        return new_entry;
    }

    // Create a new entry
    new_entry = calloc(1, sizeof(hash_map_entry));
    new_entry->key = malloc(sizeof(game));
    memcpy(new_entry->key, g, sizeof(game));

    // We initialize the number of next positions to -1
    new_entry->n_next_positions = -1;
    
    // Add the new entry to the linked list
    new_entry->next = entry;
    *hm[hash] = new_entry;

    return new_entry;
}

/*****************************************************
 * Function: hash_map_remove
 * Purpose: Removes a key from the hash map
 * Parameters: A pointer to the hash map, the key
 * Returns: None
 ****************************************************/
void hash_map_remove(hash_map *hm, game *g) {
    uint8_t hash = hash_map_hash(g);
    hash_map_entry *entry = (*hm)[hash];
    hash_map_entry *prev_entry = NULL;
    
    // Iterate through the linked list until we find the key
    while (entry != NULL) {
        if (memcmp(entry->key, g, sizeof(game)) == 0) {
            // If we found the key, remove it
            if (prev_entry == NULL) {
                (*hm)[hash] = entry->next;
            } else {
                prev_entry->next = entry->next;
            }

            game_free(entry->key);
            free(entry);
            
            return;
        }
        prev_entry = entry;
        entry = entry->next;
    }
}


/*****************************************************
 * Function: hash_map_hash
 * Purpose: Hashes a key
 * Parameters: A pointer to the key
 * Returns: The hash value
 ****************************************************/
uint8_t hash_map_hash(game *g) {
    uint8_t hash = 0;

    uint8_t sum = BOARD_SIZE * 2;

    for (int i = 0; i < BOARD_SIZE; i++) {
        hash ^= g->board.b_1d[i];
        sum += g->board.b_1d[i] * (i % 2 ? -1 : 1);
    }

    return hash * sum % HASH_MAP_SIZE; // Modulo is not really necessary since it's a uint8_t and HAS_MAP_SIZE is 2**8
}