
#ifndef COMPUTE_INTERFACE_H
#define COMPUTE_INTERFACE_H

#include "../game.h"
#include <signal.h>


struct sigaction *get_old_sigaction();

game *get_current_position();

void init_computer(int n_processes, game *g);
void clear_computer();

uint8_t computer_move_front();
void computer_move_back();


void apply_move_front(uint8_t move);
void apply_move_back();

void kill_children();

void computer_loop();

#endif