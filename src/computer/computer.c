#define _XOPEN_SOURCE 700

#include "computer.h"
#include "computer_internals.h"
#include "../data_structures/linked_list.h"
#include "../ui/ui.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>


struct sigaction old_sigaction;

int n_processes;
game *current_position;

int *pipes_fd;
int *pipe_fd_parent2child;
int *pipe_fd_child2parent;

linked_list* children;


/******************************************************************************
 * Function: get_old_sigaction
 * Description: Returns the old sigaction struct
 * Parameters: None
 * Returns: old_sigaction
 ******************************************************************************/
struct sigaction *get_old_sigaction() {
    return &old_sigaction;
}


/*****************************************************
 * Function: get_current_position
 * Purpose: Returns the current position
 * Parameters: None
 * Returns: The current position
 *****************************************************/
game *get_current_position() {
    return current_position;
}

/*****************************************************
 * Function: init_computer
 * Purpose: Sets up the computer
 * Parameters: int n_processes - the number of processes to use
 *             game *g - the initial position
 * Returns: None
 ****************************************************/
void init_computer(int n_proc, game *g) {
    // Set the number of processes
    n_processes = n_proc;
    
    // Malloc the pipes (2 per process, which means 4 fildes)
    pipes_fd = malloc(sizeof(int) * n_processes * 4);

    // Set the current position
    current_position = malloc(sizeof(game));
    memcpy(current_position, g, sizeof(game));

	// init chained list for pids
	children = new_c_list();
    
    // Fork the processes
    for (int i = 0; i < n_processes; i++) {
        // Create the pipes `parent -> child`
        pipe(pipes_fd + i * 4);

        // Create the pipes `child -> parent`
        pipe(pipes_fd + i * 4 + 2);

		// Keep children pids
		int cpid;
        
        if ((cpid = fork()) == 0) {
            // Close the pipes of the other processes
            for (int j = 0; j < i * 4; j++)
                close(pipes_fd[j]);

			// Reset SIGINT action
			struct sigaction garbage;
			sigaction(SIGINT, get_old_sigaction(), &garbage);

            setup_computer();

            pipe_fd_parent2child = pipes_fd + i * 4;
            pipe_fd_child2parent = pipes_fd + i * 4 + 2;

            close(pipe_fd_child2parent[0]);
            close(pipe_fd_parent2child[1]);

            // Set the pipe non blocking for child reading from the parent
            fcntl(pipe_fd_parent2child[0], F_SETFL, O_NONBLOCK);

            computer_loop();
            
            // Clear everything
            clear_computer();

            break;
        } else add_list(children, cpid);
    }
}


/*****************************************************
 * Function: clear_computer
 * Purpose: Clears the computer
 * Parameters: None
 * Returns: None
 ****************************************************/
void clear_computer() {
    // Close the pipes
    if (pipe_fd_child2parent != NULL && pipe_fd_parent2child != NULL) {
        close(pipe_fd_parent2child[0]);
        close(pipe_fd_child2parent[1]);
        
        exit(1);
    } else {
        for (int i = 0; i < n_processes * 4; i++)
            close(pipes_fd[i]);
    }

    // Free the pipes
    free(pipes_fd);
    pipes_fd = NULL;

    // Free the current position
    free(current_position);
    current_position = NULL;

    // Clear the cache
    clear_cache();

	// Wait children
	kill_children();
}


/*****************************************************
 * Function: compputer_move_front
 * Purpose: Gets the computer's move from the front
 * Parameters: None
 * Returns: The computer's move
 ****************************************************/
uint8_t computer_move_front() {
    int tot_n_wins[BOARD_SIZE / 2] = {0};
    int tot_n_loses[BOARD_SIZE / 2] = {0};
    int tot_n_games[BOARD_SIZE / 2] = {0};
    
    // Ask the backs for their stats on each possible move
    for (int i = 0; i < n_processes; i++) {
        // Write the move to the pipe
        write(pipes_fd[4 * i + 1], "c", 1); // `c` stands for `computer`

        // Read the stats from the pipe
        for (int j = 0; j < BOARD_SIZE / 2; j++) {
            int buffer;
            
            read(pipes_fd[4 * i + 2], &buffer, sizeof(int));
            tot_n_wins[j] += buffer;
            
            read(pipes_fd[4 * i + 2], &buffer, sizeof(int));
            tot_n_loses[j] += buffer;

            read(pipes_fd[4 * i + 2], &buffer, sizeof(int));
            tot_n_games[j] += buffer;
        }
    }
    
    // Find the best move
    int best_move = -1;
    float best_win_rate = -1;
    
    for (int i = 0; i < BOARD_SIZE / 2; i++) {
        if (tot_n_games[i] == 0)
            continue;

        float win_rate = (float) (tot_n_wins[i] - tot_n_loses[i]) / tot_n_games[i];
        
        if (win_rate >= best_win_rate) {
            best_move = i;
            best_win_rate = win_rate;
        }
    }
    
    if (best_move == -1) {
        // If there is no best move, there is a problem
        perror("No best move found");
        exit(-1);
    }

    int offset = current_position->player_turn ? BOARD_SIZE / 2 : 0;

    apply_move_front(offset + best_move);

    // Return the best move
    return best_move;
}


/*****************************************************
 * Function: computer_move_back
 * Purpose: Gets the computer's move from the back
 * Parameters: None
 * Returns: The computer's move
 ****************************************************/
void computer_move_back() {
    hash_map_entry *hm_entry = get_position_evaluation(current_position);
    
    // For each possible move, send the move to the front
    for (int i = 0; i < BOARD_SIZE / 2; i++) {
        // If the move is not possible, send 0 0 0
        if (hm_entry->next_positions[i] == NULL) {
            int buffer = 0;
            write(pipe_fd_child2parent[1], &buffer, sizeof(int));
            write(pipe_fd_child2parent[1], &buffer, sizeof(int));
            write(pipe_fd_child2parent[1], &buffer, sizeof(int));
        }
        else {
            // Otherwise, send the move and the stats
            write(pipe_fd_child2parent[1], &hm_entry->next_positions[i]->n_wins, sizeof(int));
            write(pipe_fd_child2parent[1], &hm_entry->next_positions[i]->n_loses, sizeof(int));
            write(pipe_fd_child2parent[1], &hm_entry->next_positions[i]->n_games, sizeof(int));
        }
    }
}


/*****************************************************
 * Function: apply_move_front
 * Purpose: Applies the move from the front
 * Parameters: int move - the move to apply
 * Returns: None
 ****************************************************/
void apply_move_front(uint8_t move) {
    // Send the move to the backs
    for (int i = 0; i < n_processes; i++) {
        write(pipes_fd[i * 4 + 1], "m", 1);             // `m` stands for `move`
        write(pipes_fd[i * 4 + 1], &move, sizeof(uint8_t));
    }

    // Apply the move to the current position
    if (player_move(current_position, move)) {
        perror("[FRONT] Invalid move");
        exit(-1);
    }
}


/*****************************************************
 * Function: apply_move_back
 * Purpose: Applies the move from the back
 * Parameters: None
 * Returns: None
 ****************************************************/
void apply_move_back() {
    // Get the move from the front
    uint8_t move;
    read(pipe_fd_parent2child[0], &move, sizeof(uint8_t));

    int n_balls = current_position->n_balls_left;

    // Apply the move to the current position
    if (player_move(current_position, move)) {
        perror("[BACK] Invalid move");
        exit(-1);
    }

    // If the number of balls changed, clear the unused cache
    if (n_balls != current_position->n_balls_left)
        for (int i = current_position->n_balls_left+1; i < n_balls; i++)
            clear_cache_entry(i);
}


/*****************************************************
 * Function: computer_loop
 * Purpose: The computer's loop (in the back)
 * Parameters: None
 * Returns: None
 ****************************************************/
void computer_loop() {
    // Loop until the game is over
    while (!check_end(current_position)) {
        // Check if we've received something from the front
        char action;
        
        if (read(pipe_fd_parent2child[0], &action, 1) > 0) {
            // If we've received a move, apply it
            if (action == 'm') {
                apply_move_back();
            }
            // If we've received a computer move, apply the procedure
            else if (action == 'c') {
                computer_move_back();
            }
        }

        find_a_path(get_position_evaluation(current_position), 100);
    }
}

/*****************************************************
 * Function: kill_children
 * Purpose: Kill and wait children on quit
 * Parameters: None
 * Returns: None
 ****************************************************/
void kill_children() {
    if (children == NULL)
        return;

    linked_list_node *child = children->first;
    while (child) {
		kill(child->pid, SIGINT);
		waitpid(child->pid, NULL, 0);
		child = child->next;
	}

	clear_list(children);
    children = NULL;
}
