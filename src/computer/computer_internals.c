#include "computer_internals.h"
#include "../data_structures/hash_map.h"
#include "../game.h"

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>


// Table of pointers to hash maps
hash_map* hash_maps[BOARD_SIZE * 4 + 1]; // For each number of balls left, we have a hash map
                                     // Usefull for clearing the hash maps when we have less balls left


/*****************************************************
 * Function: setup
 * Purpose: Initializes the computer
 * Parameters: None
 * Returns: None
 *****************************************************/
void setup_computer() {
    setup_cache();
    srand(time(NULL) * getpid());
}


/*****************************************************
 * Function: setup_cache
 * Purpose: Sets up the cache for the computer
 * Parameters: None
 * Returns: None
 ****************************************************/
void setup_cache() {
    // For each possible number of balls left, we create a hash map
    for (int i = 0; i <= BOARD_SIZE * 4; i++)
        hash_maps[i] = hash_map_new();
}


/*****************************************************
 * Function: clear_cache_entry
 * Purpose: Clears the cache entry for the given number of balls left
 * Parameters: int n_balls_left - the number of balls left
 * Returns: None
 ****************************************************/
void clear_cache_entry(int n_balls_left) {
    hash_map_free(hash_maps[n_balls_left]);
    hash_maps[n_balls_left] = NULL;
}

/*****************************************************
 * Function: clear_cache
 * Purpose: Clears the cache for the computer
 * Parameters: None
 * Returns: None
 ****************************************************/
void clear_cache() {
    // For each possible number of balls left, we free the hash map
    for (int i = 0; i <= BOARD_SIZE * 4; i++)
        clear_cache_entry(i);
}


/*****************************************************
 * Function: reset_cache
 * Purpose: Resets the cache for the computer
 * Parameters: None
 * Returns: None
 ****************************************************/
void reset_cache() {
    // Clear the cache
    clear_cache();

    // Setup the cache again
    setup_cache();
}


/*****************************************************
 * Function: get_position_evaluation
 * Purpose: Gets the evaluation of a position
 * Parameters: A pointer to the game
 * Returns: A pointer to the hash map entry
 ****************************************************/
hash_map_entry *get_position_evaluation(game *g) {
    // Get the number of balls left
    int n_balls_left = g->n_balls_left;

    // Get the hash map
    hash_map *hm = hash_maps[n_balls_left];
    
    if (hm == NULL) {
        perror("Hash map is NULL");
        exit(1);
    }

    // Create the hash map entry if it doesn't exist, otherwise return the existing one
    hash_map_entry *hm_entry = hash_map_put(hm, g);

    return hm_entry;
}
 

/*****************************************************
 * Function: find_next_positions
 * Purpose: Finds the next positions of the given one
 * Parameters: A pointer to the game
 * Returns: None
 ****************************************************/
void find_next_positions(hash_map_entry *hm_entry) {
    // If `hm_entry->n_next_positions` is not -1, we don't need to do anything
    if (hm_entry->n_next_positions != -1) {
        return;
    }
    
    hm_entry->n_next_positions = 0;
    
    // If this is the player's turn, we set the offset to 6
    int offset = hm_entry->key->player_turn ? 6 : 0;
    
    // For each possible next move, check if it is legal
    for (int i = 0; i < BOARD_SIZE / 2; i++){
        // If the place is not empty, the move is legal
        if (hm_entry->key->board.b_1d[offset + i] != 0) {
            game next_pos;
            memcpy(&next_pos, hm_entry->key, sizeof(game));

            if (player_move(&next_pos, offset + i)){
                perror("Player move failed");
                exit(1);
            }

            hm_entry->n_next_positions++;
            hm_entry->next_positions[i] = get_position_evaluation(&next_pos);
        }
    }
}


/*****************************************************
 * Function: update_position
 * Purpose: Updates the position in the cache
 * Parameters: A pointer to the game
 * Returns: None
 ****************************************************/
void update_position(hash_map_entry *hm_entry) {
    // First, recalculate the number of games and wins
    hm_entry->n_games = 0;
    hm_entry->n_wins = 0;
    hm_entry->n_loses = 0;

    // And check if the position is complete
    hm_entry->is_complete = true;
    
    // And we count again the number of next positions (in case of completed positions)
    hm_entry->n_next_positions = 0;
    
    for (int i = 0; i < BOARD_SIZE / 2; i++){
        if (hm_entry->next_positions[i] != NULL) {
            hm_entry->n_games += hm_entry->next_positions[i]->n_games;
            hm_entry->n_wins += hm_entry->next_positions[i]->n_wins;
            hm_entry->n_loses += hm_entry->next_positions[i]->n_loses;
            hm_entry->is_complete &= hm_entry->next_positions[i]->is_complete;
            
            if (!hm_entry->next_positions[i]->is_complete) {
                hm_entry->n_next_positions++;
            }
        }
    }
}


/*****************************************************
 * Function: find_a_path
 * Purpose: Finds a path from the given position
 * Parameters: A pointer to the game
 * Returns: None
 ****************************************************/
void find_a_path(hash_map_entry *hm_entry, int max_depth) {
    // If the position is an end game, we set n_games to 1
    if (check_end(hm_entry->key))
    {
        hm_entry->n_games = 1;
        hm_entry->is_complete = true;
        
        if (hm_entry->key->computer_score > hm_entry->key->player_score) {
            hm_entry->n_wins = 1;
            hm_entry->n_loses = 0;
        }
        else if (hm_entry->key->computer_score < hm_entry->key->player_score) {
            hm_entry->n_wins = 0;
            hm_entry->n_loses = 1;
        } else {
            hm_entry->n_wins = 0;
            hm_entry->n_loses = 0;
        }

        return;
    }

    if (max_depth == 0) 
        return;

    // If we don't know the next positions, we find them
    if (hm_entry->n_next_positions == -1) {
        find_next_positions(hm_entry);
    }

    update_position(hm_entry);

    // If the position is complete, we don't need to do anything
    if (hm_entry->is_complete) {
        return;
    }

    // Pick a random next position
    int random_index = rand() % hm_entry->n_next_positions;
    
    // Get the next position
    for (int i = 0; i < BOARD_SIZE / 2; i++){
        if (hm_entry->next_positions[i] != NULL
                && !hm_entry->next_positions[i]->is_complete) {
            if (random_index == 0) {
                // We found the next position
                find_a_path(hm_entry->next_positions[i], max_depth - 1);
                
                update_position(hm_entry);
                return;
            }
            random_index--;
        }
    }
}
