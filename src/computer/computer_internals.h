#ifndef COMPUTER_H
#define COMPUTER_H

#include "../data_structures/hash_map.h"

void setup_computer();

void setup_cache();
void reset_cache();
void clear_cache_entry(int n_balls_left);
void clear_cache();

hash_map_entry *get_position_evaluation(game *g);

void find_next_positions(hash_map_entry *hm_entry);

void update_position(hash_map_entry *hm_entry);

void find_a_path(hash_map_entry *hm_entry, int max_depth);

#endif
