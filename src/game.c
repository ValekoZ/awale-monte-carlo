#include "game.h"

#include <stdlib.h>
#include <stdio.h>


/*****************************************************
 * Function: game_new
 * Purpose: Creates a new game
 * Parameters: None
 * Returns: A pointer to the new game
 ****************************************************/
game *game_new(bool player_first) {
    game *g = malloc(sizeof(game));
    
    // Init board
    for (int i = 0; i < BOARD_SIZE; i++) {
        g->board.b_1d[i] = 4;
    }
    
    // Init player turn
    g->player_turn = player_first;
    
    // Init player scores
    g->player_score = 0;
    g->computer_score = 0;
    
    g->n_balls_left = BOARD_SIZE * 4;
    
    return g;
}


/*****************************************************
 * Function: game_free
 * Purpose: Frees the memory allocated to a game
 * Parameters: A pointer to the game to free
 * Returns: None
 ****************************************************/
void game_free(game *g) {
    free(g);
}


/*****************************************************
 * Function: player_move
 * Purpose: Moves the player's pieces
 * Parameters: A pointer to the game to move and the cell to move from
 * Returns: 0 if the move was successful, 1 if the move was invalid
 ****************************************************/
int player_move(game *g, uint8_t move){
    // Check if the move is valid
    if (g->board.b_1d[move] == 0) {
        return 1;
    }
    
    // Check if the move is between 0 and BOARD_SIZE - 1
    if (move > BOARD_SIZE - 1) {
        return 1;
    }
    
    // Check if the player can do this move
    if (g->player_turn && move < BOARD_SIZE / 2) {
        return 1;
    }
    if (!g->player_turn && move >= BOARD_SIZE / 2) {
        return 1;
    }

    uint8_t dest = move;
    
    uint8_t balls_to_move = g->board.b_1d[move];
    g->board.b_1d[move] = 0;

    // Move the pieces
    while (balls_to_move > 0) {
        balls_to_move--;
        g->board.b_1d[dest = dest ? dest - 1 : BOARD_SIZE - 1]++;
    }

    uint8_t *score_to_update = NULL;

    // Capture pieces if possible
    if (g->player_turn && dest < BOARD_SIZE / 2)
        score_to_update = &g->player_score;
    else if (!g->player_turn && dest >= BOARD_SIZE / 2)
        score_to_update = &g->computer_score;
    
    if (score_to_update != NULL) {
        while (g->board.b_1d[dest] == 3 || g->board.b_1d[dest] == 2) {
            *score_to_update += g->board.b_1d[dest];
            g->n_balls_left -= g->board.b_1d[dest];
            g->board.b_1d[dest] = 0;
            dest = (dest + 1) % BOARD_SIZE;
        }
    }
    
    // Update player turn
    g->player_turn = !g->player_turn;

    return 0;
}


/*****************************************************
 * Function: check_end
 * Purpose: Checks if the game is over
 * Parameters: A pointer to the game to check
 * Returns: 0 if the game is not over, 1 if the game is over
 ****************************************************/
int check_end(game *g) {
    int i;

    int offset = g->player_turn ? BOARD_SIZE / 2 : 0;

    // Check if the part of the board of the player which is playing is empty
    for (i = 0; i < BOARD_SIZE / 2; i++) {
        if (g->board.b_1d[offset + i] > 0) {
            break;
        }
    }
    
    if (i == BOARD_SIZE / 2) {
        // The player who is playing has no pieces left, the opponent earn all the left balls
        if (g->player_turn)
            g->computer_score += g->n_balls_left;
        else
            g->player_score += g->n_balls_left;

        g->n_balls_left = 0;

        return 1;
    }
    
    return 0;
}