#ifndef UI_H
#define UI_H


#include "../game.h"


enum key {
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_QUIT,
    KEY_ENTER,
    KEY_EMPTY,
};


enum menu_choice {
    MENU_PLAY = 0,
    MENU_SETTINGS = 1,
    MENU_QUIT = 2,
};


void ui_init(void);
void ui_quit(void);

enum key ui_get_key();

enum menu_choice ui_main_menu();
void ui_settings(void);
void game_print(game *g, int player_selection);
void play_game(void);

#endif