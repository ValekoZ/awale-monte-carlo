#ifndef UI_INTERNALS_H
#define UI_INTERNALS_H


enum color
{
    COLOR_BLACK,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_YELLOW,
    COLOR_BLUE,
    COLOR_MAGENTA,
    COLOR_CYAN,
    COLOR_WHITE,
    COLOR_DEFAULT,
};

void ui_internals_topleft();
void ui_internals_clear();
void ui_internals_move(int x, int y);
void ui_internals_set_color(enum color color);
void ui_internals_set_bg_color(enum color color);
void ui_internals_set_bold();
void ui_internals_set_underline();
void ui_internals_set_reverse();
void ui_internals_reset_color();

void ui_non_blocking_get_key();

void ui_internals_get_term_size(int *width, int *height);

#endif