#include "ui.h"
#include "ui_internals.h"
#include "../computer/computer.h"

#include <stdlib.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>


static struct termios oldt;


/*****************************************************
 * Function: ui_init
 * Purpose: Initializes the UI
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_init() {
    // Save the terminal settings
    tcgetattr(STDIN_FILENO, &oldt);
    
    // Disable echo
    struct termios newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    
    // Enter in alternate screen mode
    printf("\033[?1049h");

    // Hide the cursor
    printf("\033[?25l");
    
    // Move to the top left corner
    printf("\033[H");
}


/*****************************************************
 * Function: ui_quit
 * Purpose: Quits the UI
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_quit() {
    // Restore the terminal settings
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    
    // Show the cursor
    printf("\033[?25h");
    
    // Exit from alternate screen mode
    printf("\033[?1049l");
    
    // Make stdin blocking again
    fcntl(STDIN_FILENO, F_SETFL, 0);
}


/*****************************************************
 * Function: ui_get_key
 * Purpose: Gets a key from the user
 * Parameters: None
 * Returns: The character
 *****************************************************/
enum key ui_get_key() {
    char key;
    
    // Get the key
    if (read(STDIN_FILENO, &key, 1) == -1)
        return KEY_EMPTY;

    // Convert the key to enum value
    switch (key) {
        case 'w':
            return KEY_UP;
        case 's':
            return KEY_DOWN;
        case 'a':
            return KEY_LEFT;
        case 'd':
            return KEY_RIGHT;
        case 'q':
            return KEY_QUIT;
        case '\n':
            return KEY_ENTER;
        case '\033':
            // Escape == quit, arrow keys == move
            if (read(STDIN_FILENO, &key, 1) != -1) {
                if (key == 0x5b) {
                    read(STDIN_FILENO, &key, 1);
                    switch (key) {
                        case 'A':
                            return KEY_UP;
                        case 'B':
                            return KEY_DOWN;
                        case 'D':
                            return KEY_LEFT;
                        case 'C':
                            return KEY_RIGHT;
                    }
                }
            }
            else return KEY_QUIT;
    }

    return KEY_EMPTY;
}


/*****************************************************
 * Function: ui_main_menu
 * Purpose: Shows the main menu
 * Parameters: None
 * Returns: The selected option
 *****************************************************/
enum menu_choice ui_main_menu() {
    enum key key = KEY_EMPTY;
    
    int choice = MENU_PLAY;

    while (key != KEY_QUIT) {
        // Clear the screen
        ui_internals_clear();

        // Move to the top left corner
        ui_internals_topleft();
        
        // Set the color
        ui_internals_reset_color();

        // Print the menu as follows:
        /*
         * > Play
         * Settings
         * Quit
         */
        
        // Print the menu
        if (choice == MENU_PLAY){
            ui_internals_set_bold();
            ui_internals_set_bg_color(COLOR_GREEN);
            ui_internals_set_color(COLOR_BLACK);
        }
        else
            ui_internals_set_color(COLOR_GREEN);
        printf("Play\n");
        ui_internals_reset_color();
        
        if (choice == MENU_SETTINGS) {
            ui_internals_set_bold();
            ui_internals_set_bg_color(COLOR_YELLOW);
            ui_internals_set_color(COLOR_BLACK);
        }
        else
            ui_internals_set_color(COLOR_YELLOW);
        printf("Settings\n");
        ui_internals_reset_color();
        
        if (choice == MENU_QUIT){
            ui_internals_set_bold();
            ui_internals_set_bg_color(COLOR_RED);
        }
        else
            ui_internals_set_color(COLOR_RED);
        printf("Quit\n");
        ui_internals_reset_color();

        key = ui_get_key();

        switch (key) {
            case KEY_UP:
                choice--;
                if (choice < 0)
                    choice = MENU_QUIT;
                break;
            case KEY_DOWN:
                choice = (choice + 1) % (MENU_QUIT + 1);
                break;
            case KEY_ENTER:
            case KEY_RIGHT:
                return choice;
            case KEY_QUIT:
                return MENU_QUIT;
            case KEY_LEFT:
            case KEY_EMPTY:
                break;
        }
    }

    return MENU_QUIT;
}



/*****************************************************
 * Function: ui_settings
 * Purpose: Shows the settings menu
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_settings() {

}


/*****************************************************
 * Function: game_print
 * Purpose: Prints the game to the console
 * Parameters: A pointer to the game to print
 * Returns: None
 ****************************************************/
void game_print(game *g, int player_selection) {
    ui_internals_clear();
    ui_internals_topleft();
    
    // Print the scores
    ui_internals_set_bold();
    ui_internals_set_color(COLOR_GREEN);
    printf("Player score: %d\n", g->player_score);

    ui_internals_set_color(COLOR_RED);
    printf("Computer score: %d\n\n", g->computer_score);
    ui_internals_reset_color();
    

    // Print the turn
    if (g->player_turn) {
        ui_internals_set_bold();
        ui_internals_set_color(COLOR_GREEN);
        printf("Player's turn\n\n");
        ui_internals_reset_color();
    }
    else {
        ui_internals_set_bold();
        ui_internals_set_color(COLOR_RED);
        printf("Computer's turn\n\n");
        ui_internals_reset_color();
    }


    for (int j = 0; j < BOARD_SIZE / 2; j++) {
        printf("| %2d ", g->board.b_2d[0][j]);
    }
    printf("|\n");
    
    for (int j = 0; j < BOARD_SIZE / 2; j++) {
        printf("+----");
    }
    printf("+\n");

    for (int j = BOARD_SIZE / 2 - 1; j >= 0; j--) {
        printf("|");
        
        if (j == player_selection && g->player_turn)
        {
            ui_internals_set_bg_color(g->board.b_2d[1][j] != 0 ? COLOR_GREEN : COLOR_RED);
            ui_internals_set_color(g->board.b_2d[1][j] != 0 ? COLOR_BLACK : COLOR_WHITE);
            ui_internals_set_bold();
        }
        printf(" %2d ", g->board.b_2d[1][j]);
        ui_internals_reset_color();
    }
    printf("|\n");
}


/*****************************************************
 * Function: play_game
 * Purpose: Plays the game
 * Parameters: None
 * Returns: None
 *****************************************************/
void play_game() {
    int n_processes = 8;
    
    game *g = game_new(true);
    
    init_computer(n_processes, g);

    game_free(g);
    g = NULL;

    while (check_end(get_current_position()) == 0) {
        int move = BOARD_SIZE / 2 - 1;

        enum key pressed_key = KEY_EMPTY;

        // Get the move
        while (pressed_key != KEY_ENTER) {
            game_print(get_current_position(), move);
            pressed_key = ui_get_key();
            switch (pressed_key) {
                case KEY_UP:
                case KEY_RIGHT:
                    move--;
                    if (move < 0)
                        move = BOARD_SIZE / 2 - 1;
                    break;
                case KEY_LEFT:
                case KEY_DOWN:
                    move = (move + 1) % (BOARD_SIZE / 2);
                    break;
                case KEY_ENTER:
                    break;
                case KEY_QUIT:
					kill_children();
                    return;
                case KEY_EMPTY:
                    break;
            }
        }
        
        move += BOARD_SIZE / 2;

        if (move < BOARD_SIZE / 2 || move > BOARD_SIZE - 1 || get_current_position()->board.b_1d[move] == 0) {
            continue;
        }
        
        apply_move_front(move);

        game_print(get_current_position(), 0);

        if (check_end(get_current_position()) == 1)
            break;

        sleep(5);

        computer_move_front();
    }

    if (get_current_position()->player_score == get_current_position()->computer_score)
        printf("Tie\n");
    else
        printf("%s wins\n", get_current_position()->player_score > get_current_position()->computer_score ? "Player" : "Computer");
    
    printf("Player score: %d\n", get_current_position()->player_score);
    printf("Computer score: %d\n", get_current_position()->computer_score);
    
    clear_computer();
}