#include "ui_internals.h"

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

/*****************************************************
 * Function: ui_internals_topleft
 * Purpose: Moves the cursor to the top left corner
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_topleft() {
    printf("\033[H");
}


/*****************************************************
 * Function: ui_internals_clear
 * Purpose: Clears the screen
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_clear() {
    printf("\033[2J");
}


/*****************************************************
 * Function: ui_internals_move
 * Purpose: Moves the cursor to the given position
 * Parameters:
 *      x - The x position
 *      y - The y position
 * Returns: None
 *****************************************************/
void ui_internals_move(int x, int y) {
    printf("\033[%d;%dH", y, x);
}


/*****************************************************
 * Function: ui_internals_set_color
 * Purpose: Sets the color of the text
 * Parameters:
 *      color - The color
 * Returns: None
 *****************************************************/
void ui_internals_set_color(enum color color) {
    switch (color) {
        case COLOR_BLACK:
            printf("\033[30m");
            break;
        case COLOR_RED:
            printf("\033[31m");
            break;
        case COLOR_GREEN:
            printf("\033[32m");
            break;
        case COLOR_YELLOW:
            printf("\033[33m");
            break;
        case COLOR_BLUE:
            printf("\033[34m");
            break;
        case COLOR_MAGENTA:
            printf("\033[35m");
            break;
        case COLOR_CYAN:
            printf("\033[36m");
            break;
        case COLOR_WHITE:
            printf("\033[37m");
            break;
        case COLOR_DEFAULT:
            printf("\033[39m");
            break;
    }
}


/*****************************************************
 * Function: ui_internals_set_bg_color
 * Purpose: Sets the background color of the text
 * Parameters:
 *      color - The color
 * Returns: None
 *****************************************************/
void ui_internals_set_bg_color(enum color color) {
    switch (color) {
        case COLOR_BLACK:
            printf("\033[40m");
            break;
        case COLOR_RED:
            printf("\033[41m");
            break;
        case COLOR_GREEN:
            printf("\033[42m");
            break;
        case COLOR_YELLOW:
            printf("\033[43m");
            break;
        case COLOR_BLUE:
            printf("\033[44m");
            break;
        case COLOR_MAGENTA:
            printf("\033[45m");
            break;
        case COLOR_CYAN:
            printf("\033[46m");
            break;
        case COLOR_WHITE:
            printf("\033[47m");
            break;
        case COLOR_DEFAULT:
            printf("\033[49m");
            break;
    }
}


/*****************************************************
 * Function: ui_internals_set_bold
 * Purpose: Sets the text to bold
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_set_bold() {
    printf("\033[1m");
}


/*****************************************************
 * Function: ui_internals_set_underline
 * Purpose: Sets the text to underline
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_set_underline() {
    printf("\033[4m");
}


/*****************************************************
 * Function: ui_internals_set_reverse
 * Purpose: Sets the text to reverse
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_set_reverse() {
    printf("\033[7m");
}


/*****************************************************
 * Function: ui_internals_reset_color
 * Purpose: Resets the color of the text
 * Parameters: None
 * Returns: None
 *****************************************************/
void ui_internals_reset_color() {
    printf("\033[0m");
}


/*****************************************************
 * Function: ui_non_blocking_get_key
 * Purpose: Gets a key without blocking
 * Parameters: None
 * Returns: The key
 *****************************************************/
void ui_non_blocking_get_key() {
    // Make stdin non-blocking
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}


/*****************************************************
 * Function: ui_internals_get_term_size
 * Purpose: Gets the size of the terminal
 * Parameters:
 *     width - The width of the terminal
 *    height - The height of the terminal
 * Returns: None
 * Notes:
 *    This function write the width and height to the
 *    parameters.
 *****************************************************/
void ui_internals_get_term_size(int *width, int *height) {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    *width = w.ws_col;
    *height = w.ws_row;
}