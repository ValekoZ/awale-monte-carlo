#ifndef GAME_H
#define GAME_H

#include <stdbool.h>
#include <stdint.h>

#define BOARD_SIZE 12

union board {
    int b_2d[2][BOARD_SIZE/2];
    int b_1d[BOARD_SIZE];
};

typedef struct game_t
{
    union board board;
    bool player_turn;   // true = player, false = computer
    uint8_t player_score;
    uint8_t computer_score;
    uint8_t n_balls_left;
} game;


game *game_new(bool player_first);


void game_free(game *g);


int player_move(game *g, uint8_t move);


int check_end(game *g);


#endif
