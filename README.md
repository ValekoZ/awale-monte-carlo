# AWALE - Monte Carlo AI

![Build status](https://valekoz.gitlab.io/awale-monte-carlo/build.svg) ![Tests status](https://valekoz.gitlab.io/awale-monte-carlo/test.svg)

## Introduction

### What is awale?

Awale is a game played on a board with 2x6 hollow, with two players.

At the beginning of the game, each hollow is filled with 4 seeds.

![Starting position in the game of awale](awale.jpg)

- Move: In turn, the player takes all the seeds from one of the 6 holes on their side and counterclockwise deposits one in each hole following the one they have emptied.
- Capture: If the last seed is placed in one of the 6 holes on the opponent's side and already has 1 or 2 seeds, the player captures the resulting 2 or 3 seeds. The captured seeds are then taken out of the play (attic) and the hole is left empty.
- Roundup: When a player grabs 2 or 3 seeds, if the previous cell also contains 2 or 3 seeds, they are also captured and so on.

The game ends when a player has no more seeds in his camp because he can no longer play. The opponent then captures the remaining seeds (on his side). The player with the most seeds captured wins. Additional rules exist.

[![asciicast](https://asciinema.org/a/7k5NtgJ7rmwgrCC8ohanS1ium.svg)](https://asciinema.org/a/7k5NtgJ7rmwgrCC8ohanS1ium)

## Build

### Dependencies

In order to build the project, you need to install the following dependencies:

- [A C compiler](https://en.wikipedia.org/wiki/C_compiler) (the project was tested with Clang)
- [Meson](https://mesonbuild.com/)

#### Debian / Ubuntu / PopOS / etc.

```bash
sudo apt-get update
sudo apt-get install clang meson
```

#### Arch Linux / Manjaro / etc.

```bash
sudo pacman -Sy clang meson
```

### Build the project

You can setup the build by running the following command:

```bash
meson builddir
```

When this is done, any changes to `meson.build` will be automatically reflected in the build directory.

You can then build the project by running the following command:

```bash
meson compile -C builddir
```

### Install the project

If you want to install the project, you can run the following command:

```bash
meson install -C builddir
```

You can then run the project by running the following command:

```bash
awale
```

### Test the project

We didn't implement any test for the project yet. But if we did, we would run the following command:

```bash
meson test -C builddir
```

You can add more tests by adding a `test_` file in the `tests/` directory, importing all the necessary modules and writing the test in the `main` function.

You will then have to update the `meson.build` file to include the new test:

```meson
test_with_explicit_name = executable('test_with_explicit_name', ['tests/test_with_explicit_name.c', 'src/...'])
test('Explicit name', test_with_explicit_name)
```

You can refer to [this page](https://mesonbuild.com/Unit-tests.html) for more information about unit testing in Meson.

## Context

This project was created for a class project in the Artificial Intelligence course at TELECOM Nancy.

The project was created by:

- [Gauthier HOUILLON (gautzer)](https://gitlab.com/gautzer)
- [Adrien BONTEMS (kaoronne)](https://gitlab.com/adrien.bontems)
- [Lucas VALENTIN (ValekoZ)](https://gitlab.com/valekoz)

## Contribute

Since this is a class project, we didn't really think about maintaining it after the end of the course.

But if you want to contribute, you can do so by opening an issue or a pull request on [GitLab](https://gitlab.com/ValekoZ/awale-monte-carlo/-/issues), and we will try to add your contribution.

## License

This project is licensed under the [MIT license](LICENSE.md). This means that you can use it for any purpose, including commercial purposes, without having to pay any license fees.
